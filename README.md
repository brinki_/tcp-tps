

Ciao Brini ecco qui come avrei intenzione di strutturare l'app:

L' applicazione si apre con una schermata in cui sono presenti due pulsanti: 
 - *avvia una partita*
 - *unisciti ad una partita*
uno dei due giocatori avvia una partita, generando un codice che l'altro dovrà inserire dopo aver clocato su *unisciti ad una partita*


I due giocatori posseggono quindi lo stesso codice e possono quindi essere facilmente gestiti dal server.


Il server genera un numero casuale (0 o 1) per decidere quale dei due giocatori inizia per primo, e invia all'applicazione un messaggio per comunicare chi deve giocare.


Poi da qui dovrebbe essere piuttosto semplice sviluppare il resto

Fammi sapere cosa ne pensi
