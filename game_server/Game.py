import json
import math
import random
#--------------------------------------------------------------------------------------------------------------------------------------------

class Game:

#--------------------------------------------------------------------------------------------------------------------------------------------

    def __init__(self):
        self.__playerOnePosition = 0
        self.__playerTwoPosition = 0

        self.__playerOneScore = 0
        self.__playerTwoScore = 0

        self.__playerOneStatus = 0
        self.__playerTwoStatus = 0

        self.__ballSpeed = 1
        self.__ballDirection = [0,0]

        self.__winner = 0

        self.gameReady = False

        self.__SCREEN_WIDTH = 900
        self.__SCREEN_HEIGHT = 650

        self.__SPEED = 5
        self.__PAD_SIZE = 80

        self.__ballDirection = [10, 10]
        self.__ballPosition = [self.__SCREEN_WIDTH/2, self.__SCREEN_HEIGHT/2]

        self.__enableNewDirection = True

#--------------------------------------------------------------------------------------------------------------------------------------------

    def updatePlayerStatus(self, player, status):
        if(player == 1):
            self.__playerOneStatus = status
        elif(player == 2):
            self.__playerTwoStatus = status
        else:
            return "invalidPlayer"

        if(self.__playerOneStatus == 1 and self.__playerTwoStatus == 1):
            self.gameReady = True
        else:
            self.gameReady = False

#--------------------------------------------------------------------------------------------------------------------------------------------

    def __updatePlayerPos(self, player, position):
        if(player == 1):
            self.__playerOnePosition = position
        elif(player == 2):
            self.__playerTwoPosition = position
        else:
            return {
                'status' : 'error'
            }

        return {
            'status' : 'successful',
            'playerOnePosition' : self.__playerOnePosition,
            'playerTwoPosition' : self.__playerTwoPosition
        }

#--------------------------------------------------------------------------------------------------------------------------------------------

    def __updateScore(self, playerOneScore, playerTwoScore):
        pass

#--------------------------------------------------------------------------------------------------------------------------------------------

    def __updateWinner(self):
        
        if(self.__playerOneScore == 5):
            self.__winner = 1
        if(self.__playerTwoScore == 5):
            self.__winner = 2

#--------------------------------------------------------------------------------------------------------------------------------------------
    def __updateBall(self):
        self.__ballPosition[0] += self.__ballDirection[0]
        self.__ballPosition[1] += self.__ballDirection[1]

        if self.__ballPosition[0] < 40 and (self.__playerTwoPosition > self.__ballPosition[1] or self.__playerTwoPosition + self.__PAD_SIZE < self.__ballPosition[1]):
            print("PLAYER 2 LOSES")
            
        if self.__ballPosition[0] > self.__SCREEN_WIDTH-40 and (self.__playerOnePosition > self.__ballPosition[1] or self.__playerOnePosition + self.__PAD_SIZE < self.__ballPosition[1]):
            print("PLAYER 1 LOSES")


        if (self.__ballPosition[0] < 40 or self.__ballPosition[0] > self.__SCREEN_WIDTH - 40) and self.__enableNewDirection == True:

            self.__enableNewDirection = False

            maxRandValue = int(math.sqrt(math.pow(self.__SPEED,2) / 1.44))

            print("MAX RAND: {0}".format(maxRandValue))

            self.__ballDirection[0] = - self.__ballDirection[0]

            if self.__ballDirection[1] > 0:
                self.__ballDirection[1] = random.randint(0, maxRandValue)
            else:
                self.__ballDirection[1] = random.randint(-maxRandValue, 0)

            print(self.__ballDirection[1])
            
            neg = False
            if self.__ballDirection[0] < 0:
                neg = True

            self.__ballDirection[0] = math.sqrt(math.pow(self.__SPEED, 2) - math.pow(self.__ballDirection[1], 2))
            if neg == True:
                self.__ballDirection[0] = -self.__ballDirection[0]
                pass

            actual_speed = math.sqrt(math.pow(self.__ballDirection[0], 2) + math.pow(self.__ballDirection[1], 2))

            print("ACTUAL SPEED IS: {0}".format(actual_speed))


        if self.__ballPosition[1] <= 20 or self.__ballPosition[1] >= self.__SCREEN_HEIGHT-20:
            self.__ballDirection[1] = - self.__ballDirection[1]

        if self.__ballPosition[0] > 30 and self.__ballPosition[0] < self.__SCREEN_WIDTH-30:
            self.__enableNewDirection = True


#--------------------------------------------------------------------------------------------------------------------------------------------

    def getGameData(self):

        return json.dumps({
            "game" : {
                "winner" : self.__winner,
                "points" : {
                    "player1" : int(self.__playerOneScore),
                    "player2" : int(self.__playerTwoScore)
                },
                "ready" : self.gameReady,
                "players" : {
                    "player1" : {
                        "status" : self.__playerOneStatus,
                        "position" : {
                            "y" : int(self.__playerOnePosition)
                        }
                    },
                    "player2" : {
                        "status" :  self.__playerTwoStatus,
                         "position" : {
                            "y" : int(self.__playerTwoPosition)
                        }
                    }
                },
                "ball" : {
                    "position" :  {
                        "x" : self.__ballPosition[0],
                        "y" : self.__ballPosition[1]
                    },
                    "speedValue" : self.__SPEED,
                    "speedVector" : {
                        "x" : self.__ballDirection[0],
                        "y" : self.__ballDirection[1]
                    }
                }
            }
        })

    def updateGameValues(self, playerNumber, playerPos):
        self.__updatePlayerPos(playerNumber, playerPos)
        self.__updateBall()

#--------------------------------------------------------------------------------------------------------------------------------------------
    
    def resetGame(self):
        self.__playerOnePosition = [0,0]
        self.__playerTwoPosition = [0,0]

        self.__playerOneScore = 0
        self.__playerTwoScore = 0

        self.__ballSpeed = 0
        self.__ballDirection = [0,0]

        self.__winner = 0

        self.gameReady = False