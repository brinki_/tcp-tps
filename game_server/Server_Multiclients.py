import socket
from _thread import start_new_thread

class Server:

#--------------------------------------------------------------------------------------------------------------------------------------------

    def __init__(self, host, port, max_conns):

        self.__socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM) #istanzio socket che accetta indirizzi IPv4 e connessioni TCP 
        self.__max_players = max_conns

        try:
            self.__socket.bind((host, port))
        except socket.error as e:
            print('errore nell\'aprire il server, magari la porta è già utilizzata')
            print(str(e))

#--------------------------------------------------------------------------------------------------------------------------------------------

    def startServer(self):
        self.__socket.listen(self.__max_players) #server in ascolto: numero di connessioni massime = max_conns

#--------------------------------------------------------------------------------------------------------------------------------------------

    def startGameService(self):
        self.__currentPlayer = 0

        while True:
            print('waiting for players...')

            conn, addr = self.__socket.accept()
            print("Player connected: ", addr)
            
            self.__currentPlayer+=1
            start_new_thread(self.__threaded_client, (conn, self.__currentPlayer))

#--------------------------------------------------------------------------------------------------------------------------------------------
    def getPlayersNumber(self):
        return len(self.__currentPlayer+1)
#--------------------------------------------------------------------------------------------------------------------------------------------

    def __threaded_client(self, conn, currentPlayer):
        conn.send(str.encode("Connected"))
        reply = ""
        while True:
            try:
                data = conn.recv(1024)
                reply = data.decode("utf-8")

                if not data:
                    print("Disconnected")
                    break
                else:
                    print("Received from player %d: %s" % (currentPlayer, reply))
                    print("Sending to player %d: %s" % (currentPlayer, reply))

                conn.sendall(str.encode(reply))
            except:
                break

        print("Lost connection")
        conn.close()



