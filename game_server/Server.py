import socket
import json
from _thread import start_new_thread
from Game import Game

class Server:

#--------------------------------------------------------------------------------------------------------------------------------------------

    def __init__(self, host, port, max_conns):

        self.__socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM) #istanzio socket che accetta indirizzi IPv4 e connessioni TCP 
        self.__max_players = max_conns
        self.__game = Game()
        try:
            self.__socket.bind((host, port))
        except socket.error as e:
            print('errore nell\'aprire il server, magari la porta è già utilizzata')
            print(str(e))

#--------------------------------------------------------------------------------------------------------------------------------------------

    def startServer(self):
        self.__socket.listen() #server in ascolto: numero di connessioni massime = max_conns

#--------------------------------------------------------------------------------------------------------------------------------------------

    def startGameService(self):
        self.__currentPlayer = 0

        while True:
            print('waiting for players...')

            conn, addr = self.__socket.accept()
            print("Player connected: ", addr)
            
            self.__currentPlayer+=1
            start_new_thread(self.__threaded_client, (conn, self.__currentPlayer))

#--------------------------------------------------------------------------------------------------------------------------------------------
    def getPlayersNumber(self):
        return len(self.__currentPlayer+1)
#--------------------------------------------------------------------------------------------------------------------------------------------

    def __threaded_client(self, conn, currentPlayer):
       
        self.__game.updatePlayerStatus(currentPlayer, 1)

        conn.send(
            json.dumps({
                'status' : 'connected',
                'playerNumber' : int(currentPlayer)
            }).encode('utf-8')
        )

        print("connesso")

        while True:

            try:
                rawInputData = conn.recv(1024).decode('utf8')

                if not rawInputData:
                    print('disconneted')
                    self.__game.updatePlayerStatus(currentPlayer, 0)
                    self.__currentPlayer-=1
                    break
                    
                else:

                    print(rawInputData)
                    jsonData = json.loads(rawInputData)

                    if(jsonData['action'] == "getGameData"):

                        self.__game.updateGameValues(currentPlayer, jsonData['game']['player']['position'])

                        conn.sendall(str(self.__game.getGameData()).encode('utf-8'))
                        
                    elif(jsonData['action'] == "resetGame"):
                        self.__game.updatePlayerStatus(currentPlayer, 0)
                        self.__currentPlayer-=1
                        break
            except:
                self.__game.updatePlayerStatus(currentPlayer, 0)
                self.__currentPlayer-=1
                break

        print("Lost connection player %d" % currentPlayer)
        conn.close()



