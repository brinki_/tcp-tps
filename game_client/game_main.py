

'''
{"game": {
    "winner": 0, 
    "points": {
        "player1": 0, 
        "player2": 0
    }, 
    "ready": true, 

    "players": {

        "player1": {
            "status": 1, 
            "position": {
                "y": 350
            }
        }, 
        
        "player2": {
            "status": 1, 
            "position": {
                "y": 380
            }
        }
    }, 
    
    "ball": {
        "position": {
            "x": "<int>", 
            "y": "<int>"
        }, 
        "speedValue": "<int>", 
        "speedVector": {
            "x": "<int>", 
            "y": "<int>"
        }
    }
}}'

'''



# Francesco Gritti - 24/11/2020
#
# Multiplayer Pong Game 


SERVER_IP = "80.116.8.43"
SERVER_PORT = 5555




DISABLE_CONNECTION = False


# Import modules
import pygame
import json

import socket
import time 
import sys
import random
import math

# Import pygame.locals for easier access to key events
from pygame.locals import (K_UP, K_DOWN, K_ESCAPE, KEYDOWN, QUIT, )


random.seed()


# screen width and height
SCREEN_WIDTH = 900
SCREEN_HEIGHT = 650

BALL_SIZE = 15

SPEED = 20
PAD_SIZE = 80

ballDirection = [10, 10]
ballPosition = [SCREEN_WIDTH/2, SCREEN_HEIGHT/2]



# Define a player object by extending pygame.sprite.Sprite
# The surface drawn on the screen is now an attribute of 'player'

class Player(pygame.sprite.Sprite):
    def __init__(self, left, top, width, height):
        super(Player, self).__init__()
        self.surf = pygame.Surface((width, height))
        self.surf.fill((255, 255, 255))
        self.rect = self.surf.get_rect()

        self.rect.left = left
        self.rect.top = top
    
    def update(self, pressed_keys):

        if pressed_keys[K_UP] and self.rect.top >= 24:
            self.rect.move_ip(0, -10)

        if pressed_keys[K_DOWN] and self.rect.bottom <= SCREEN_HEIGHT-24:
            self.rect.move_ip(0, 10)


    def setPosition (self, xPos = -1, yPos = -1):

        if xPos == -1:
            xPos = self.rect.left
        if yPos == -1:
            yPos = self.rect.top

        self.rect.top = yPos
        self.rect.left = xPos




# Initialize pygame
pygame.init()


# Create the screen object
screen = pygame.display.set_mode((SCREEN_WIDTH, SCREEN_HEIGHT))



# Instantiate players, payer is the player controlled by the user,
# otherPlayer is controlled by the other player 
player = Player(SCREEN_WIDTH - 40, 10, 15, PAD_SIZE)
otherPlayer = Player(25, 10, 15, PAD_SIZE)

# add an event triggered every 10 ms for reading the user action for moving up
# or down the pad
MOVE_PAD = pygame.USEREVENT + 1
pygame.time.set_timer(MOVE_PAD, 10)


# Variable to keep the main loop running
running = True
enableNewDirection = True


# init a clock for slowing down the loop
clock = pygame.time.Clock()



# connect to the server and read the player number
playerNumber = 0
clientSocket1 = None

if DISABLE_CONNECTION == False:
    clientSocket1 = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    clientSocket1.connect((SERVER_IP, SERVER_PORT))


    raw_data = clientSocket1.recv(1024)
    while True:
        if raw_data != None:
            break

    print(raw_data)

    json_dict = json.loads(raw_data.decode("utf-8"))

    try:
        playerNumber = int(json_dict["playerNumber"])

    except:
        print("Invalid server response")
        tcpSocket.close()
        exit(0)

    print("Success! You are the player number: {0}".format(playerNumber))

    clientSocket1.sendall(json.dumps({"action": "getGameData", "game" : {"player" : {"position" : player.rect.top}}}).encode("utf-8"))





# Main loop
while running:

    # for loop through the event queue
    for event in pygame.event.get():

        # Check for KEYDOWN event, i.e. events triggered by the pression of a key
        if event.type == KEYDOWN:

            # If the Esc key is pressed, then exit the main loop
            if event.key == K_ESCAPE:
                running = False

        # Check for QUIT event (press the 'X' for closing the window)
        elif event.type == QUIT:
            running = False


        # if the event is the one triggered by the 10ms timer, read the keyboard
        # and if needed move the pad up or down
        elif event.type == MOVE_PAD:

            pressed_keys = pygame.key.get_pressed()
            player.update(pressed_keys)



    # ------------------- GRAPHICS ----------------------

    # draw a black background
    screen.fill((0, 0, 0))

    PADDING = 10
    BORDER_WIDTH = 4

    pygame.draw.rect(screen, (255,255,255), [PADDING, PADDING, SCREEN_WIDTH- 2*PADDING, SCREEN_HEIGHT - 2*PADDING])
    pygame.draw.rect(screen, (0,0,0), [PADDING + BORDER_WIDTH, PADDING + BORDER_WIDTH, SCREEN_WIDTH-2*(PADDING + BORDER_WIDTH), SCREEN_HEIGHT-2*(PADDING + BORDER_WIDTH)])
    pygame.draw.rect(screen, (255,255,255), [(SCREEN_WIDTH/2)- BORDER_WIDTH/2 ,PADDING + BORDER_WIDTH, BORDER_WIDTH, SCREEN_HEIGHT-2*(PADDING + BORDER_WIDTH)])


    #draw the ball
    pygame.draw.circle(screen, (50,50,200), (ballPosition[0], ballPosition[1]), BALL_SIZE)

    # Draw the player on the screen
    screen.blit(player.surf, player.rect)
    screen.blit(otherPlayer.surf, otherPlayer.rect)

    # Update the display
    pygame.display.flip()




    # -------------------------- Server Communication ---------------------------

    if DISABLE_CONNECTION == False:

        clientSocket1.sendall(json.dumps({"action": "getGameData", "game" : {"player" : {"position" : player.rect.top}}}).encode("utf-8"))

        raw_data = clientSocket1.recv(1024)

        while True:
            if raw_data != None:
                break

        if raw_data != None:

            print(raw_data)
            try:
                json_dict = json.loads(raw_data.decode("utf-8"))
                playerNumber_ID = "player2"
                
                if playerNumber == 2:
                    playerNumber_ID = "player1"

                print("{0} Player ID string: {1}".format(playerNumber, playerNumber_ID))

                otherPlayer_yPos = int(json_dict["game"]["players"][playerNumber_ID]["position"]["y"])
                otherPlayer.setPosition(-1, otherPlayer_yPos)

                print(otherPlayer_yPos)

                ballPosition[0] = int(json_dict["game"]["ball"]["position"]["x"])

                if playerNumber == 2:
                    ballPosition[0] = SCREEN_WIDTH - ballPosition[0]

                ballPosition[1] = int(json_dict["game"]["ball"]["position"]["y"])

            except:
                print("invalid message!")



    clock.tick(35)
    


tcpSocket.close()






'''
    # ---------------------------- Move the ball --------------------------------

    ballPosition[0] += ballDirection[0]
    ballPosition[1] += ballDirection[1]



    
     #   DETECT IF A PLAYER MISSES THE BALL:
     #
     #   if  ballPosition[0] < 40 and (player2_posY > ballPosition[1] or player2_posY + PAD_SIZE < ballPositon[1]):
     #       print("PLAYER 2 LOSES")
     #   
     #   if ballPosition[0] > SCREEN_WIDTH-40 and (player1_posY > ballPosition[1] or player1_posY + PAD_SIZE < ballPositon[1]):
     #       print("PLAYER 1 LOSES")



    if  ballPosition[0] < 40 and (otherPlayer.rect.top > ballPosition[1] or otherPlayer.rect.top + PAD_SIZE < ballPosition[1]):
        print("PLAYER 2 LOSES")
        
    if ballPosition[0] > SCREEN_WIDTH-40 and (player.rect.top > ballPosition[1] or player.rect.top + PAD_SIZE < ballPosition[1]):
        print("PLAYER 1 LOSES")


    if (ballPosition[0] < 40 or ballPosition[0] > SCREEN_WIDTH - 40) and enableNewDirection == True:

        enableNewDirection = False

        maxRandValue = int(math.sqrt(math.pow(SPEED,2) / 1.44))

        print("MAX RAND: {0}".format(maxRandValue))

        ballDirection[0] = - ballDirection[0]

        if ballDirection[1] > 0:
            ballDirection[1] = random.randint(0, maxRandValue)
        else:
            ballDirection[1] = random.randint(-maxRandValue, 0)

        print(ballDirection[1])
        
        neg = False
        if ballDirection[0] < 0:
            neg = True

        ballDirection[0] = math.sqrt(math.pow(SPEED, 2) - math.pow(ballDirection[1], 2))
        if neg == True:
            ballDirection[0] = -ballDirection[0]
            pass

        actual_speed = math.sqrt(math.pow(ballDirection[0], 2) + math.pow(ballDirection[1], 2))

        print("ACTUAL SPEED IS: {0}".format(actual_speed))


    if ballPosition[1] <= 20 or ballPosition[1] >= SCREEN_HEIGHT-20:
        ballDirection[1] = - ballDirection[1]

    # this is needed to prevent some bad latch-up conditions
    if ballPosition[0] > 30 and ballPosition[0] < SCREEN_WIDTH-30:
        enableNewDirection = True

'''
