import socket

max_clients = 5

serverSocket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

serverSocket.bind(('127.0.0.1', 40000))

serverSocket.listen(max_clients)
clientSocket, address = serverSocket.accept()
print('connected')

while True:
    data = clientSocket.recv(1024)
    print(data)
    if not data:
        break
    